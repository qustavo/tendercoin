package commands

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"paradigm/tendercoin/abci"
	"strings"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"gopkg.in/urfave/cli.v1"
)

type Transfer struct {
}

func (cmd *Transfer) Command() cli.Command {
	return cli.Command{
		Name:   "transfer",
		Usage:  "Transfer --amount to a given --addr",
		Action: cmd.action,
		Flags: []cli.Flag{
			cli.StringFlag{Name: "priv-key"},
			cli.StringFlag{Name: "addr"},
			cli.Uint64Flag{Name: "amount"},
		},
	}
}

func (cmd *Transfer) action(c *cli.Context) error {
	privKey := c.String("priv-key")
	// strip 0x if presetn
	if strings.HasPrefix(privKey, "0x") {
		privKey = privKey[2:]
	}
	prv, err := crypto.HexToECDSA(privKey)
	if err != nil {
		return fmt.Errorf("Parsing private key: %v", err)
	}

	addr := c.String("addr")
	if addr == "" {
		return errors.New("addr can't be empty")
	}

	amount := c.Uint64("amount")
	if amount == 0 {
		return errors.New("amount can't be 0")
	}

	t, err := abci.NewTransfer(prv, common.HexToAddress(addr), amount)
	if err != nil {
		return err
	}

	b64, err := abci.EncodeTransfer(t)
	if err != nil {
		return err
	}

	url := fmt.Sprintf(`http://localhost:26657/broadcast_tx_commit?tx="%s"`, string(b64))
	log.Printf("GET %s", url)
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	log.Printf("Status: %d, Body: %s", resp.StatusCode, string(body))
	return nil
}
