package memdb

import (
	"paradigm/tendercoin/store"
	"sync"
)

type MemDB struct {
	m       sync.RWMutex
	balance map[string]store.Amount
}

func New() *MemDB {
	return &MemDB{
		balance: make(map[string]store.Amount),
	}
}

func (s *MemDB) Fund(addr string, amount store.Amount) error {
	s.m.Lock()
	defer s.m.Unlock()

	s.balance[addr] += amount
	return nil
}

func (s *MemDB) Balance(addr string) (store.Amount, error) {
	s.m.RLock()
	defer s.m.RUnlock()

	return s.balance[addr], nil
}

func (s *MemDB) Transfer(from, to string, amount store.Amount) error {
	s.m.Lock()
	defer s.m.Unlock()

	balance := s.balance[from]
	if amount > balance {
		return store.ErrNotEnoughFunds
	}

	s.balance[from] -= amount
	s.balance[to] += amount

	return nil
}
