module paradigm/tendercoin

require (
	github.com/allegro/bigcache v1.1.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/ethereum/go-ethereum v1.8.20
	github.com/go-kit/kit v0.8.0 // indirect
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/gogo/protobuf v1.2.0 // indirect
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/pkg/errors v0.8.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2
	github.com/syndtr/goleveldb v0.0.0-20181128100959-b001fa50d6b2 // indirect
	github.com/tendermint/go-amino v0.14.1 // indirect
	github.com/tendermint/tendermint v0.27.0
	github.com/urfave/cli v1.20.0
	golang.org/x/net v0.0.0-20181207154023-610586996380 // indirect
	google.golang.org/grpc v1.17.0 // indirect
	gopkg.in/urfave/cli.v1 v1.20.0
)
