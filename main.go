package main

import (
	"paradigm/tendercoin/commands"

	"gopkg.in/urfave/cli.v1"
)

func main() {
	app := cli.NewApp()
	app.Commands = []cli.Command{
		(&commands.ABCI{}).Command(),
		(&commands.Balance{}).Command(),
		(&commands.Generate{}).Command(),
		(&commands.Transfer{}).Command(),
	}

	app.RunAndExitOnError()
}
