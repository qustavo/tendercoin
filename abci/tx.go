package abci

import (
	"crypto/ecdsa"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
)

type Tx struct {
	Op   string          `json:"op"`
	Body json.RawMessage `json:"body"`
}

type Transfer struct {
	Pub     string `json:"pubkey"`
	Address string `json:"address"`
	Sig     string `json:"sig"`
	Amount  uint64 `json:"amount"`
}

func NewTransfer(prv *ecdsa.PrivateKey, address common.Address, amount uint64) (*Transfer, error) {
	t := &Transfer{
		Pub:     hexutil.Encode(crypto.FromECDSAPub(&prv.PublicKey)),
		Address: address.Hex(),
		Amount:  amount,
	}

	sig, err := crypto.Sign(t.Hash(), prv)
	if err != nil {
		return nil, err
	}
	t.Sig = hex.EncodeToString(sig)

	return t, nil
}

func (t *Transfer) Verify(pub *ecdsa.PublicKey) bool {
	sig, err := hex.DecodeString(t.Sig)
	if err != nil {
		panic(err)
	}

	return crypto.VerifySignature(
		crypto.FromECDSAPub(pub),
		t.Hash(),
		sig[:len(sig)-1], // remove recovery id
	)
}

func (t *Transfer) Hash() []byte {
	return crypto.Keccak256([]byte(
		fmt.Sprintf("%s%s%d", t.Pub, t.Address, t.Amount),
	))
}

func (t *Transfer) String() string {
	return fmt.Sprintf("Transfer:\n Pub: %s\n Address: %s\n Amount: %d", t.Pub, t.Address, t.Amount)
}

func EncodeTransfer(t *Transfer) ([]byte, error) {
	body, err := json.Marshal(t)
	if err != nil {
		return nil, err
	}

	tx, err := json.Marshal(&Tx{
		Op:   "transfer",
		Body: body,
	})
	if err != nil {
		return nil, err
	}

	enc := base64.StdEncoding.EncodeToString(tx)
	return []byte(enc), nil
}

func (t *Transfer) PubKey() *ecdsa.PublicKey {
	dec, err := hexutil.Decode(t.Pub)
	if err != nil {
		panic(err)
	}

	pub, err := crypto.UnmarshalPubkey(dec)
	if err != nil {
		panic(err)
	}

	return pub
}

func DecodeTransfer(enc []byte) (*Transfer, error) {
	i, err := ParseTx(enc)
	if err != nil {
		return nil, err
	}

	// TODO: better type handling
	return i.(*Transfer), nil
}

func ParseTx(v []byte) (interface{}, error) {
	enc := base64.StdEncoding
	dbuf := make([]byte, enc.DecodedLen(len(v)))
	n, err := enc.Decode(dbuf, v)
	if err != nil {
		return nil, err
	}

	var tx Tx
	if err := json.Unmarshal(dbuf[:n], &tx); err != nil {
		return nil, err
	}

	var obj interface{}
	switch tx.Op {
	case "transfer":
		obj = &Transfer{}
	}

	if err := json.Unmarshal(tx.Body, obj); err != nil {
		return nil, err
	}

	return obj, nil
}
